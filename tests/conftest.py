import pytest
from chat.utils import authenticate

from chat.app_factory import create_app
from chat.models import db


@pytest.yield_fixture()
def app():
    """Comments application fixture."""
    flask_app = create_app(config_object='chat.config.TestingConfig')
    # Remove auth function (already tested separately)
    flask_app.before_request_funcs[None].remove(authenticate)
    # Push app context to let db make its things
    flask_app.app_context().push()

    # Prevent tests from failing due to existing test database
    # (you might have cancelled previous test suite or something)
    db.drop_all()
    db.create_all()

    yield flask_app

    # Prevent transaction from hanging in the `idle` state
    db.session.rollback()
    db.session.remove()

    db.drop_all()