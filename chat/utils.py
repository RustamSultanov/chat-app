import jwt
import functools
import os

from flask import current_app, request, g
from flask import jsonify as flask_jsonify
from jwt import ExpiredSignatureError
from werkzeug.exceptions import Unauthorized, Forbidden


def authenticate():
    g.user = {'id': 1}
    return
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return

    auth_token = auth_header.split(' ')[1]
    secret_key = current_app.config.get('SECRET_KEY')
    try:
        user = jwt.decode(auth_token, secret_key,
                          algorithms=['HS256'])
    except ExpiredSignatureError:
        raise Unauthorized('Token has expired')

    except Exception as e:
        current_app.logger.exception(e)
        raise Unauthorized('Invalid token')

    if 'id' not in user:
        raise Unauthorized('Invalid token')

    g.user = user

def login_required(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        user = getattr(g, 'user', None)
        if user:
            return f(*args, **kwargs)
        raise Unauthorized()
    return decorated_function


class BaseConfig:
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.getenv('SECRET_KEY', 'dummy_secret_key')
    REDIS_CONNECT_TIMEOUT = 0.2
    METRICS_URL = os.getenv('METRICS_URL', '/metrics')
    METRICS_ENDPOINT = os.getenv('METRICS_ENDPOINT', 'metrics')
    METRICS_TEMP_DIR = os.getenv('METRICS_TEMP_DIR', '/tmp')
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONN_STRING')


class BaseServiceConfig:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONN_STRING')
    SQLALCHEMY_ECHO = 'SQLALCHEMY_ECHO' in os.environ

    REDIS_URL = os.getenv('REDIS_URL', 'redis://redis:6379')

    SENTRY_DSN = os.getenv('SENTRY_DSN')


class BaseDevConfig(BaseConfig):
    DEBUG = True


class BaseTestConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_CONN_STRING')


def jsonify(data, status_code=200):
    response = {
        'status_code': status_code,
    }
    if status_code in (200, 201):
        response['data'] = data
        response['status'] = 'success'
    else:
        response['error'] = data
        response['status'] = 'error'

    return flask_jsonify(response), status_code
