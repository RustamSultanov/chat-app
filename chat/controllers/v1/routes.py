from flask import Blueprint

from chat.controllers.v1.messages import MessagesResource

comments_view = MessagesResource.as_view('messages')

public_bp = Blueprint('messages', __name__)

public_bp.add_url_rule(
    rule='/units/<int:unit_id>/messages',
    view_func=comments_view,
    methods={'POST', 'GET'}
)

public_bp.add_url_rule(
    rule='/message/<int:message_id>',
    view_func=comments_view,
    methods=('DELETE', ) )
